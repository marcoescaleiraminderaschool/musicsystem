# Music System
 
  - Is catalog of music tracks
  - Persists Music tracks
  - Should retrieve the name, album, genre, duration and the artists of a track
  - Should retrieve the year, name, genre and a list of tracks of an album
  - Should list albums by year (ordered by most recent)
  - Should list albums by genre
  - Find tracks, artists or albums by name
 
## Getting Started
 
 These instructions will get you a copy of the project up and running on your local machine for development.
 
### Prerequisites
 
 What things you need to install the software and how to install them
 
 ```
 Node
 Yarn
 ```
 
### Installing
 
First off start by cloning the project
 
 ```
 git clone https://bitbucket.org/marcoescaleira/musicsystem.git
 ```
 
 Acess project folder
 
 ```
 cd musicsystem
 ```
  
 Install all the packages
 
 ```
 yarn
 ```
   
 Start development server
 
 ```
 yarn start
 ```
   
 Start json-server
 
 ```
 cd json-server
 yarn startDB
 ```
 
 Then you can access *http://localhost:3000/* and have fun!
 
## ER Model

![ERModel](https://vrl-eu-cdn.wetransfer.net/ivise/eyJwaXBlbGluZSI6W1sic3JnYiIse31dLFsiYXV0b19vcmllbnQiLHt9XSxbImdlb20iLHsiZ2VvbWV0cnlfc3RyaW5nIjoiNTEyeDUxMj4ifV0sWyJzaGFycGVuIix7InJhZGl1cyI6MC43NSwic2lnbWEiOjAuNX1dLFsiZXhwaXJlX2FmdGVyIix7InNlY29uZHMiOjYwNDgwMH1dXSwic3JjX3VybCI6InMzOi8vd2V0cmFuc2Zlci1ldS1wcm9kLW91dGdvaW5nLzVjMGMwNDc5N2I3NDI1N2VjNjYzZTNiN2RkMTcxZWMyMjAxOTAyMjcxMjM0NTUvN2Y5MzU1ZjE3ZjNhMzcwZmViNWVlOGFmZDQ5MWYxNDg1MWE4ZjYxNSJ9/ae14254f02096d1be3ab7bc4ba746190f45049c47e921f91ffd1416c94471bb1)
 
## Built With
 
 * [React JS](https://reactjs.org/) - The javascript framework used
 * [Redux](https://redux.js.org/) - The state container used
 * [Json-Server](https://github.com/typicode/json-server) - The server used
 * [SCSS](https://sass-lang.com/) - CSS pre-processor
 * [Yarn](https://yarnpkg.com/lang/en/) - Dependency Management

## Author
 
 * **Marco Escaleira** - [MarcoEscaleira](https://github.com/marcoescaleira)
