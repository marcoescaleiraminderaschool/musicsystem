import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions, selectors } from '../store/albums';

class AlbumForm extends Component {
  state = {
    name: (this.props.album.name) ? this.props.album.name : '',
    year: (this.props.album.year) ? this.props.album.year : '',
    error: ''
  };

  onNameChange = (e) => {
    const name = e.target.value;
    this.setState(() => ({name}));
  };

  onYearChange = (e) => {
    const year = e.target.value;
    if(!year || /^\d+$/.test(year)) {
      this.setState(() => ({year}));
    }
  };

  onSubmit = (e) => {
    e.preventDefault();
    
    const {name, year} = this.state;
    
    if(this.props.albums.filter((album) => album.name.toLowerCase() === this.state.name.toLowerCase()).length > 0 
        &&
       this.props.albums[this.props.album.id-1].name.toLowerCase() === this.state.name.toLowerCase()) {
      this.setState(() => ({ error: 'There is already an album with that name!' }));
    } else if(!name || !year) {
      this.setState(() => ({ error: 'Name and Year can\'t be empty!' }));
    } else {
      this.setState(() => ({ error: '' }));
      this.props.onSubmit({
        name,
        year
      });
    }
  };
  
  componentDidMount() {
    this.props.loadAlbums();
  }

  render () {
    return (
      <div className="container__body">
        <form
          onSubmit={this.onSubmit}
          className="form"
        >
          { this.state.error && <p className="form__error">{this.state.error}</p> }
          <input 
            type="text"
            placeholder="Name"
            value={this.state.name}
            onChange={this.onNameChange}
            className="form__input"
          />
          <input 
            type="text"
            placeholder="Year"
            value={this.state.year}
            onChange={this.onYearChange}
            className="form__input"
          />
          <input
            type="submit"
            value={this.props.editing ? 'Update' : 'Add'}
            className="btn--submit"
          />
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  albums: selectors.getAllAlbums(state)
});

const mapDispatchToProps = {
  loadAlbums: actions.loadAlbums
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumForm);

