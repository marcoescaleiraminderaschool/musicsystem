import React from 'react';
import { connect } from 'react-redux';
import { API_URL } from '../config';

import { selectors } from '../store/genres';


class AlbumTracks extends React.Component {
  state = {
    tracks: []
  }

  componentDidMount() {
    this.setState(() => ({ tracks: [] }));

    const getTracks = async () => {
      const request = await fetch(`${API_URL}/albums/${this.props.albumId}/tracks`);
      const tracks = await request.json();
      this.setState(() => ({ tracks }));
    }
    getTracks();
  }

  render() {
    const { tracks } = this.state;
  
    return (
      <div>
        <h2>Tracks</h2>
        {
          tracks.length > 0 ? 
            tracks.map(track => {
              const genreName = this.props.genres[track.genreId-1].name;
              return (
                <h3 key={track.id}>{track.name} ({genreName})</h3>
              );
            }) :
            'No tracks in this album'            
        }
      </div>
    );
  };
}

AlbumTracks.defaultState = {
  tracks: []
};

const mapStateToProps = state => ({
  genres: selectors.getAllGenres(state)
});

export default connect(mapStateToProps)(AlbumTracks);