import React, { Component } from 'react';
import { connect } from "react-redux";
import { actions, selectors } from '../store/artists';

class ArtistForm extends Component {
  state = {
    name: (this.props.artist.name) ? this.props.artist.name : '',
    error: ''
  };

  onNameChange = (e) => {
    const name = e.target.value;
    this.setState(() => ({name}));
  };

  onSubmit = (e) => {
    e.preventDefault();
    
    const {name} = this.state;

    if(this.props.artists.filter((artist) => artist.name.toLowerCase() === this.state.name.toLowerCase()).length > 0
        &&
       this.props.artists[this.props.artist.id-1].name.toLowerCase() === this.state.name.toLowerCase()) {
      this.setState(() => ({ error: 'There is already an artist with that name!' }));
    } else if(!name) {
      this.setState(() => ({ error: 'Name can\'t be empty!' }));
    } else {
      this.setState(() => ({ error: '' }))
      this.props.onSubmit({
        name
      });
    }
  };

  componentDidMount() {
    this.props.loadArtists();
  };

  render () {
    return (
      <div className="container__body">
        <form
          onSubmit={this.onSubmit}
          className="form"
        >
          { this.state.error && <p className="form__error">{this.state.error}</p> }
          <input 
            type="text"
            placeholder="Name"
            value={this.state.name}
            onChange={this.onNameChange}
            className="form__input"
          />
          <input
            type="submit"
            value={this.props.editing ? 'Update' : 'Add'}
            className="btn--submit"
          />
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  artists: selectors.getAllArtists(state)
});

const mapDispatchToProps = {
  loadArtists: actions.loadArtists
};

export default connect(mapStateToProps, mapDispatchToProps)(ArtistForm);

