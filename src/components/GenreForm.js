import React, { Component } from 'react';
import { connect } from "react-redux";
import { actions, selectors } from "../store/genres";

class GenreForm extends Component {
  state = {
    name: (this.props.genre.name) ? this.props.genre.name : '',
    error: ''
  };

  onNameChange = (e) => {
    const name = e.target.value;
    this.setState(() => ({name}));
  };

  onSubmit = (e) => {
    e.preventDefault();
    
    const {name} = this.state;

    if(this.props.genres.filter((genre) => genre.name.toLowerCase() === this.state.name.toLowerCase()).length > 0
        &&
       this.props.genres[this.props.genre.id-1].name.toLowerCase() === this.state.name.toLowerCase()) {
      this.setState(() => ({ error: 'There is already an genre with that name!' }));
    } else if(!name) {
      this.setState(() => ({ error: 'Name can\'t be empty!' }));
    } else {
      this.setState(() => ({ error: '' }));
      this.props.onSubmit({
        name
      });
    }
  };

  componentDidMount() {
    this.props.loadGenres();
  };

  render () {
    return (
      <div className="container__body">
        <form
          onSubmit={this.onSubmit}
          className="form"
        >
          { this.state.error && <p className="form__error">{this.state.error}</p> }
          <input
            type="text"
            placeholder="Name"
            value={this.state.name}
            onChange={this.onNameChange}
            className="form__input"
          />
          <input
            type="submit"
            value={this.props.editing ? 'Update' : 'Add'}
            className="btn--submit"
          />
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  genres: selectors.getAllGenres(state)
});

const mapDispatchToProps = {
  loadGenres: actions.loadGenres
}

export default connect(mapStateToProps, mapDispatchToProps)(GenreForm);

