import React from 'react';
import { Link } from "react-router-dom";

const Header = () => (
  <header className="header">
    <nav className="header__nav">
      <Link to="/" className="nav__title">
        Music <span className="nav__title__span">System</span>
      </Link>
      <ul className="nav__ul">
        <li className="nav__ul__li">
          <Link to="/" className="nav__ul__li__link">Home</Link>
        </li>
        <li className="nav__ul__li">
          <Link to="/tracks" className="nav__ul__li__link">Tracks</Link>
        </li>
        <li className="nav__ul__li">
          <Link to="/albums" className="nav__ul__li__link">Albums</Link>
        </li>
        <li className="nav__ul__li">
          <Link to="/artists" className="nav__ul__li__link">Artists</Link>
        </li>
        <li className="nav__ul__li">
          <Link to="/genres" className="nav__ul__li__link">Genres</Link>
        </li>
      </ul>
    </nav>
  </header>
);

export default Header;