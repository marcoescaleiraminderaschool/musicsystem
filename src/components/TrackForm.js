import React, { Component } from 'react';
import Select from 'react-select';
import { connect } from "react-redux";
import { actions as tracksActions, selectors as tracksSelectors } from "../store/tracks";
import { actions as albumsActions, selectors as albumsSelectors } from "../store/albums";
import { actions as artistsActions, selectors as artistsSelectors } from "../store/artists";
import { actions as genresActions, selectors as genresSelectors} from "../store/genres";

const DEFAULT_STATE = {
  name: '',
  duration: '',
  albumId: '',
  artistId: '',
  genreId: '',
  error: ''
};

class TrackForm extends Component {
  state = {
    ...DEFAULT_STATE,
    name: (this.props.track.name) ? this.props.track.name : '',
    duration: (this.props.track.duration) ? this.props.track.duration : '',
    albumId: (this.props.track.albumId) ? this.props.track.albumId : '',
    artistId: (this.props.track.artistId) ? this.props.track.artistId : '',
    genreId: (this.props.track.genreId) ? this.props.track.genreId : '',
    error: ''
  };

  onNameChange = (e) => {
    const name = e.target.value;
    this.setState(() => ({name}));
  };

  onDurationChange = (e) => {
    const duration = e.target.value;
    const regEX = /^\d{1,}(:\d{0,2})?$/;
    if (!duration || duration.match(regEX)) {
      this.setState(() => ({ duration }));
    }
  };

  onAlbumChange = (selectedAlbum) => {
    this.setState(() => ({
      albumId: selectedAlbum.value
    }));
  };
  
  onArtistChange = (selectedArtist) => {
    console.log(selectedArtist);
    const artistId = selectedArtist.map((artist) => artist.value);
    this.setState(() => ({
      artistId
    }));
  };

  onGenreChange = (selectedGenre) => {
    this.setState(() => ({
      genreId: selectedGenre.value
    }));
  };

  onSubmit = (e) => {
    e.preventDefault();
    
    const {name, duration, albumId, artistId, genreId} = this.state;

    if(this.props.tracks.filter((track) => track.name.toLowerCase() === this.state.name.toLowerCase()).length > 0
        &&
       this.props.tracks[this.props.track.id-1].name.toLowerCase() !== this.state.name.toLowerCase()) {
      this.setState(() => ({ error: 'There is already a track with that name!' }));
    } else if(!name || !duration || !albumId || !artistId || !genreId) {
      this.setState(() => ({ error: 'All fields need to be filled!' }));
    } else if(duration <= 1) {
      this.setState(() => ({ error: 'Duration must be greater than 1.00 minute' }));
    } else {
      this.setState(() => ({ error: '' }));
      this.props.onSubmit({
        name,
        duration,
        albumId,
        artistId,
        genreId
      });
    }
  };

  componentDidMount() {
    this.props.loadTracks();
    this.props.loadAlbums();
    this.props.loadArtists();
    this.props.loadGenres();
  }

  render () {
    const { artists, albums, genres } = this.props;
    return (
      <div className="container__body">
        <form
          onSubmit={this.onSubmit}
          className="form"
        >
          { this.state.error && <p className="form__error">{this.state.error}</p> }
          <input
            type="text"
            placeholder="Name"
            value={this.state.name}
            onChange={this.onNameChange}
            className="form__input"
          />
          <input 
            type="text"
            placeholder="Duration"
            value={this.state.duration}
            onChange={this.onDurationChange}
            className="form__input"
          />
          <Select 
            placeholder="Select an album..."
            onChange={this.onAlbumChange}
            options={
              albums.map((album) => ({
                value: album.id,
                label: `${album.name} (${album.year})`
              }))
            }
          />
          <br />
          <Select 
            placeholder="Select the artist(s)..."
            onChange={this.onArtistChange}
            options={
              artists.map((artist) => ({
                value: artist.id,
                label: artist.name
              }))
            }
            isMulti
          />
          <br />
          <Select
            placeholder="Select an genre..."
            onChange={this.onGenreChange}
            options={
              genres.map((genre) => ({
                value: genre.id,
                label: genre.name
              }))
            }
          />
          <br />
          <input
            type="submit"
            value={this.props.editing ? 'Update' : 'Add'}
            className="btn--submit"
          />
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  tracks: tracksSelectors.getAllTracks(state),
  albums: albumsSelectors.getAllAlbums(state),
  genres: genresSelectors.getAllGenres(state),
  artists: artistsSelectors.getAllArtists(state)
});

const mapDispatchToProps = {
  loadTracks: tracksActions.loadTracks,
  loadAlbums: albumsActions.loadAlbums,
  loadArtists: artistsActions.loadArtists,
  loadGenres: genresActions.loadGenres,
}

export default connect(mapStateToProps, mapDispatchToProps)(TrackForm);

