import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { selectors as albumsFilterSelectors, actions as albumsFilterActions } from '../store/albumsFilter';
import { selectors as albumsSelectors, actions as albumsActions} from '../store/albums';

class AlbumsPage extends React.Component {
  componentDidMount() {
    this.props.loadAlbums();
  }

  render() {
    const {setFilter, textFilter, albums} = this.props;
    return (
      <div className="container">
        <div className="container__header">
          <input 
            type="text" 
            value={textFilter} 
            onChange={(e) => {
              setFilter(e.target.value);
            }}
            className="input input--search"
            placeholder="Search for a album"
          />
          <select 
            onChange={(e) => {
              switch(e.target.value) {
                case 'gender':
                  setFilter(textFilter, 'gender');
                  break;
                default:
                  setFilter(textFilter);
                  break;
              }
            }}
          >
            <option value="year">Year</option>
            <option value="genre">Genre</option>
          </select>
          <Link 
            to="/addAlbum" 
            className="btn btn--add"
          >
          Add a album
          </Link>
        </div>
        <div className="container__body">
          {
            albums.length > 0 &&
            albums.map((album) => (
              <div key={album.id}>
                <Link to={`/editAlbum/${album.id}`} className="btn btn--blackText">
                  <h3>{album.name}</h3>
                </Link>
                <p>Year: {album.year}</p>
              </div>
            ))
          }
        </div>
      </div>
    );
  }
}

AlbumsPage.defaultProps = {
  albums: []
};

const mapStateToProps = state => {
  const textFilter = albumsFilterSelectors.getTextFilter(state);
  return {
    textFilter,
    albums: textFilter ? albumsFilterSelectors.getResults(state) : albumsSelectors.getAllAlbums(state)
  }
};

const mapDispatchToProps = {
  loadAlbums: albumsActions.loadAlbums,
  setFilter: albumsFilterActions.setFilter
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumsPage);