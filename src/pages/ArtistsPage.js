import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { actions as artistsActions, selectors as artistsSelectors} from '../store/artists'
import { actions as artistsFilterActions, selectors as artistsFilterSelectors} from '../store/artistsFilter'

class ArtistsPage extends React.Component {
  componentDidMount() {
    this.props.loadArtists();
  }

  render() {
    const {artists, textFilter, setFilter} = this.props;
    return (
      <div className="container">
        <div className="container__header">
          <input
            type="text"
            value={textFilter}
            onChange={(e) => {
              setFilter(e.target.value);
            }}
            className="input input--search"
            placeholder="Search for a artist"
          />
          <Link to="/addArtist" className="btn btn--add">Add a artist</Link>
        </div>
        <div className="container__body">
          {
            artists.map( ({name, id}) => (
              <div key={id}>
                <Link to={`/editArtist/${id}`} className="btn btn--blackText">
                  <h3>{name}</h3>
                </Link>
              </div>
            ))
          }
        </div>
      </div>
    );
  };
};

const mapStateToProps = state => {
  const textFilter = artistsFilterSelectors.getTextFilter(state);
  return {
    textFilter,
    artists: textFilter ? artistsFilterSelectors.getResults(state) : artistsSelectors.getAllArtists(state),
  }
};

const mapDispatchToProps = {
  loadArtists: artistsActions.loadArtists,
  setFilter: artistsFilterActions.setFilter
};

export default connect(mapStateToProps, mapDispatchToProps)(ArtistsPage);