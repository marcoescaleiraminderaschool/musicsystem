import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { actions } from '../store/genres';

class GenresPage extends React.Component {
  componentDidMount() {
    this.props.loadGenres();
  }

  render() {
    const {genres} = this.props;
    return (
      <div className="container">
        <div className="container__header">
          <Link 
            to="/addGenre" 
            className="btn btn--add"
          >
          Add a genre
          </Link>
        </div>
        <div className="container__body">
          {
            genres.map( ({name, id}) => (
              <div key={id}>
                <Link 
                  to={`/editGenre/${id}`}
                  className="btn btn--blackText"  
                >
                  <h3>{name}</h3>
                </Link>
              </div>
            ))
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  genres: state.genres
});

const mapDispatchToProps = {
  loadGenres: actions.loadGenres
}

export default connect(mapStateToProps, mapDispatchToProps)(GenresPage);