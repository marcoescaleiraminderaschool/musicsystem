import React from 'react';
import { connect } from 'react-redux';
import { actions as tracksActions, selectors as tracksSelectors } from '../store/tracks';
import { actions as tracksFilterActions, selectors as tracksFilterSelectors } from '../store/tracksFilter';
import { actions as albumsActions, selectors as albumsSelectors } from '../store/albums';
import { actions as artistsActions, selectors as artistsSelectors } from '../store/artists';
import { actions as genresActions, selectors as genresSelectors } from '../store/genres';
import { Link } from 'react-router-dom';

class TracksPage extends React.Component {
  componentDidMount() {
    this.props.loadArtists();
    this.props.loadAlbums();
    this.props.loadGenres();
    this.props.loadTracks();
  }

  render() {
    const { tracks, albums, artists, genres, textFilter, setFilter } = this.props;

    return (
      <div className="container">
        <div className="container__header">
          <input
            type="text"
            value={textFilter}
            onChange={(e) => {
              setFilter(e.target.value);
            }}
            className="input input--search"
            placeholder="Search for a track"
          />
          <Link 
            to="/addTrack" 
            className="btn btn--add"
          >
          Add a track
          </Link>
        </div>
        <div className="container__body">
          {
            tracks.map(({ name, duration, artistId, genreId, albumId, id }) => {
              const artistsLi = artistId.map(aId => artists && <li key={aId}>{artists.find(({id}) => id === aId).name}</li>); 
              const album = albums.find(({id}) => id === albumId);
              const genre = genres.find(({id}) => id === genreId);

              return (
                <div key={id}>
                  <h3>{name}</h3>
                  <p>Duration: {parseFloat(duration).toFixed(2)}</p>
                  <p>Artist(s):</p>
                  <ul>
                    {artistsLi.map(artist => artist)}
                  </ul>
                  <p>Album: {album && album.name}</p>
                  <p>Genre: {genre && genre.name}</p>
                  <button
                    onClick={() => {
                      this.props.removeTrack(id);
                      this.props.history.push('/tracks');
                    }}
                    className="btn btn--delete"
                  >
                    Delete
                </button>
                </div>
              );
            })
          }
        </div>
      </div>
    );
  }
}

TracksPage.defaultProps = {
  tracks: [],
  albums: [],
  genres: [],
  artists: [],
}

const mapStateToProps = state => {
  const textFilter = tracksFilterSelectors.getTextFilter(state);
  return {
    textFilter,
    tracks: textFilter ? tracksFilterSelectors.getResults(state) : tracksSelectors.getAllTracks(state),
    albums: albumsSelectors.getAllAlbums(state),
    genres: genresSelectors.getAllGenres(state),
    artists: artistsSelectors.getAllArtists(state)
  }
};

const mapDispatchToProps = {
  loadTracks: tracksActions.loadTracks,
  loadAlbums: albumsActions.loadAlbums,
  loadArtists: artistsActions.loadArtists,
  loadGenres: genresActions.loadGenres,
  setFilter: tracksFilterActions.setFilter,
  removeTrack: tracksActions.removeTrack
}

export default connect(mapStateToProps, mapDispatchToProps)(TracksPage);