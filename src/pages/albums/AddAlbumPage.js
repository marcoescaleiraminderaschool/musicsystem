import React from 'react';
import { connect } from 'react-redux';
import { actions } from '../../store/albums';

import AlbumForm from '../../components/AlbumForm';

const AddArtistPage = props => (
  <div className="container">
    <div className="container__header">
      <h1 className="container__header__title">Add a album</h1>
      <button
        onClick={() => {
          props.history.push('/albums');
        }}
        className="btn btn--delete"
      >
        Go back
      </button>
    </div>
    <AlbumForm
      album={{}}
      onSubmit={album => {
        props.addAlbum(album);
        props.history.push('/albums');
      }}
    />
  </div>
);

const mapDispatchToProps = {
  addAlbum: actions.addAlbum
};

export default connect(null, mapDispatchToProps)(AddArtistPage);
