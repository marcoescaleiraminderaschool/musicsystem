import React from 'react';
import { connect } from 'react-redux';
import { actions, selectors } from '../../store/albums';

import AlbumForm from '../../components/AlbumForm';
import AlbumTracks from '../../components/AlbumTracks';

const EditAlbumPage = props => (
  <div className="container">
    <div className="container__header">
      <h1 className="container__header__title">Edit a Album</h1>
      <button
        onClick={() => {
          props.history.push('/albums');
        }}
        className="btn btn--delete"
      >
        Go back
      </button>
    </div>
    <AlbumForm
      editing
      album={props.album}
      onSubmit={album => {
        props.editAlbum(props.album.id, album);
        props.history.push('/albums');
      }}
    />
    <br />
    <button
      onClick={() => {
        props.removeAlbum(props.album.id);
        props.history.push('/albums');
      }}
      className="btn btn--delete"
    >
      Delete
    </button>
    <hr />
    <AlbumTracks 
      albumId={props.album.id}
    />
  </div>
);

EditAlbumPage.defaultProps = {
  album: {}
};

const mapStateToProps = (state, props) => {
  return {
    // eslint-disable-next-line eqeqeq
    album: selectors.getAllAlbums(state).find((album) => album.id == props.match.params.id)
}};

const mapDispatchToProps = {
  editAlbum: actions.editAlbum,
  removeAlbum: actions.removeAlbum
};


export default connect(mapStateToProps, mapDispatchToProps)(EditAlbumPage);
