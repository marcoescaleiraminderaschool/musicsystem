import React from 'react';
import { connect } from 'react-redux';
import { actions } from '../../store/artists';

import ArtistForm from '../../components/ArtistForm';

const AddArtistPage = props => (
  <div className="container">
    <div className="container__header">
      <h1 className="container__header__title">Add a artist</h1>
      <button
        onClick={() => {
          props.history.push('/artists');
        }}
        className="btn btn--delete"
      >
        Go back
      </button>
    </div>
    <ArtistForm
      artist={{}}
      onSubmit={artist => {
        props.addArtist(artist);
        props.history.push('/artists');
      }}
    />
  </div>
);

const mapDispatchToProps = {
  addArtist: actions.addArtist
};

export default connect(null, mapDispatchToProps)(AddArtistPage);
