import React from 'react';
import { connect } from 'react-redux';
import { actions, selectors } from '../../store/artists';

import ArtistForm from '../../components/ArtistForm';

const EditArtistPage = props => (
  <div className="container">
    <div className="container__header">
      <h1 className="container__header__title">Edit a Artist</h1>
      <button
        onClick={() => {
          props.history.push('/artists');
        }}
        className="btn btn--delete"
      >
        Go back
      </button>
    </div>
    <ArtistForm
      editing
      artist={props.artist}
      onSubmit={artist => {
        props.editArtist(props.artist.id, artist);
        props.history.push('/artists');
      }}
    />
    <br />
    <button
      onClick={() => {
        props.removeArtist(props.artist.id);
        props.history.push('/artists');
      }}
      className="btn btn--delete"
    >
      Delete
    </button>
  </div>
);

EditArtistPage.defaultProps = {
  artist: {}
};

const mapStateToProps = (state, props) => {
  return {
    // eslint-disable-next-line eqeqeq
    artist: selectors.getAllArtists(state).find((artist) => artist.id == props.match.params.id)
}};

const mapDispatchToProps = {
  editArtist: actions.editArtist,
  removeArtist: actions.removeArtist
};


export default connect(mapStateToProps, mapDispatchToProps)(EditArtistPage);
