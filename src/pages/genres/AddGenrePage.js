import React from 'react';
import { connect } from 'react-redux';
import { actions } from '../../store/genres';

import GenreForm from '../../components/GenreForm';

const AddGenrePage = props => (
  <div className="container">
    <div className="container__header">
      <h1 className="container__header__title">Add a genre</h1>
      <button
        onClick={() => {
          props.history.push('/genres');
        }}
        className="btn btn--delete"
      >
        Go back
      </button>
    </div>
    <GenreForm
      genre={{}}
      onSubmit={genre => {
        props.addGenre(genre);
        props.history.push('/genres');
      }}
    />
  </div>
);

const mapDispatchToProps = {
  addGenre: actions.addGenre
};

export default connect(null, mapDispatchToProps)(AddGenrePage);
