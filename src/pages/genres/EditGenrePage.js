import React from 'react';
import { connect } from 'react-redux';
import { actions, selectors } from '../../store/genres';

import GenreForm from '../../components/GenreForm';

const EditGenrePage = props => (
  <div className="container">
    <div className="container__header">
      <h1 className="container__header__title">Edit a genre</h1>
      <button
        onClick={() => {
          props.history.push('/genres');
        }}
        className="btn btn--delete"
      >
        Go back
      </button>
    </div>
    <GenreForm
      editing
      genre={props.genre}
      onSubmit={genre => {
        props.editGenre(props.genre.id, genre);
        props.history.push('/genres');
      }}
    />
    <br />
    <button
      onClick={() => {
        props.removeGenre(props.genre.id);
        props.history.push('/genres');
      }}
      className="btn btn--delete"
    >
      Delete
    </button>
  </div>
);

EditGenrePage.defaultProps = {
  genre: {}
};

const mapStateToProps = (state, props) => {
  return {
    // eslint-disable-next-line eqeqeq
    genre: selectors.getAllGenres(state).find((genre) => genre.id == props.match.params.id)
}};

const mapDispatchToProps = {
  editGenre: actions.editGenre,
  removeGenre: actions.removeGenre
};


export default connect(mapStateToProps, mapDispatchToProps)(EditGenrePage);
