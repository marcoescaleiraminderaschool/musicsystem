import React from 'react';
import { connect } from 'react-redux';
import { actions } from '../../store/tracks';

import TrackForm from '../../components/TrackForm';

const AddTrackPage = props => (
  <div className="container">
    <div className="container__header">
      <h1 className="container__header__title">Add a Track</h1>
      <button
        onClick={() => {
          props.history.push('/tracks');
        }}
        className="btn btn--delete"
      >
        Go back
      </button>
    </div>
    <TrackForm
      track={{}}
      onSubmit={track => {
        props.addTrack(track);
        props.history.push('/tracks');
      }}
    />
  </div>
);


const mapStateToProps = state => ({
  state
});

const mapDispatchToProps = {
  addTrack: actions.addTrack
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTrackPage);
