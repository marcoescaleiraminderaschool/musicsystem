import React from 'react';
import { connect } from 'react-redux';
import { actions, selectors } from '../../store/tracks';

import TrackForm from '../../components/TrackForm';

const EditTrackPage = props => (
  <div>
    <h1>Edit a Track</h1>
    <TrackForm
      editing
      track={props.track}
      onSubmit={track => {
        props.editTrack(props.track.id, track);
        props.history.push('/tracks');
      }}
    />
    <button
      onClick={() => {
        props.removeTrack(props.track.id);
        props.history.push('/tracks');
      }}
    >
      Delete Track
    </button>
  </div>
);

const mapStateToProps = (state, props) => {
  return {
    // eslint-disable-next-line eqeqeq
    track: selectors.getAllTracks(state).find((track) => track.id == props.match.params.id)
}};

const mapDispatchToProps = {
  editTrack: actions.editTrack,
  removeTrack: actions.removeTrack
};


export default connect(mapStateToProps, mapDispatchToProps)(EditTrackPage);
