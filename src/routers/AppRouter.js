import React, { Component, Fragment } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import HomePage from "../pages/HomePage";

import TracksPage from '../pages/TracksPage';
import AddTrackPage from '../pages/tracks/AddTrackPage';
import EditTrackPage from '../pages/tracks/EditTrackPage';

import AlbumsPage from '../pages/AlbumsPage';
import AddAlbumPage from '../pages/albums/AddAlbumPage';
import EditAlbumPage from "../pages/albums/EditAlbumPage";

import ArtistsPage from '../pages/ArtistsPage';
import AddArtistPage from '../pages/artists/AddArtistPage';
import EditArtistPage from '../pages/artists/EditArtistPage';

import GenresPage from '../pages/GenresPage';
import AddGenrePage from '../pages/genres/AddGenrePage';
import EditGenrePage from '../pages/genres/EditGenrePage';

import Header from "../components/Header";

class AppRouter extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
          <Header />
          <Switch>
            <Route path="/" exact component={HomePage} />
            <Route path="/tracks" component={TracksPage} />
            <Route path="/addTrack" component={AddTrackPage} />
            <Route path="/editTrack/:id" component={EditTrackPage} />
            <Route path="/albums" component={AlbumsPage} />
            <Route path="/addAlbum" component={AddAlbumPage} />
            <Route path="/editAlbum/:id" component={EditAlbumPage} />
            <Route path="/artists" component={ArtistsPage} />
            <Route path="/addArtist" component={AddArtistPage} />
            <Route path="/editArtist/:id" component={EditArtistPage} />
            <Route path="/genres" component={GenresPage} />
            <Route path="/addGenre" component={AddGenrePage} />
            <Route path="/editGenre/:id" component={EditGenrePage} />
          </Switch>
        </Fragment>
      </BrowserRouter>
    );
  }
}

export default AppRouter;