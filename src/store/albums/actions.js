import { API_URL, HEADERS } from "../../config";
import { getAllAlbums } from "./selectors";

export const loadAlbums = () => (dispatch, getState) => {
  const isInCache = getAllAlbums(getState()).length > 0;

  if (isInCache) {
    return;
  }

  fetch(`${API_URL}/albums?_sort=year&_order=desc`)
    .then(res => res.json())
    .then(albums => {
      dispatch({
        type: 'LOAD_ALBUMS',
        albums
      });
    });
};

// ADD_ALBUM
export const addAlbum = ({name = '', year = 0} = {}) => (dispatch) => {
  fetch(`${API_URL}/albums`, {
    headers: HEADERS,
    method: 'POST',
    body: JSON.stringify({
      name,
      year
    })
  })
    .then(res => res.json())
    .then(album => {
      dispatch({
        type: 'ADD_ALBUM',
        album
      });
    })
};

// REMOVE_ALBUM
export const removeAlbum = (id) => (dispatch) => {
  fetch(`${API_URL}/albums/${id}`, {
    method: 'DELETE'
  })
    .then(() => {
      dispatch({
        type: 'REMOVE_ALBUM',
        id
      })
    });
};

// EDIT_ALBUM
export const editAlbum = (id, updates) => (dispatch) => {
  fetch(`${API_URL}/albums/${id}`, {
    headers: HEADERS,
    method: 'PUT',
    body: JSON.stringify(updates)
  })
    .then(res => res.json())
    .then(updatedAlbum => {
      dispatch({
        type: 'EDIT_ALBUM',
        updatedAlbum
      })
    });
};