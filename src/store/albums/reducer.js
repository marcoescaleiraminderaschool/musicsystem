const defaultState = [];

const albumsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'LOAD_ALBUMS': 
      return action.albums;
    case 'ADD_ALBUM':
      return [
        ...state,
        action.album
      ];
    case 'REMOVE_ALBUM':
      return state.filter(({id}) => id !== action.id);
    case 'EDIT_ALBUM':
      return state.map((album) => {
        if(album.id === action.updatedAlbum.id) {
          return {
            ...album,
            ...action.updatedAlbum
          };
        } else {
          return album;
        }
      });
    default:
      return state;
  }
};

export default albumsReducer;