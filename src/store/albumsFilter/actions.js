import { API_URL } from '../../config';
import { getOrderCriteria } from './selectors';

export const setFilter = (text, sortBy = 'year') => (dispatch) => {
  dispatch({
    type: 'SET_FILTER_ALBUMS',
    text,
    sortBy
  });

  dispatch(fetchResults(text));
};

const fetchResults = (text) => (dispatch, getState) => {
  const sortBy = getOrderCriteria(getState());

  fetch(`${API_URL}/albums?q=${text}&_sort=${sortBy}&_order=desc`)
    .then(res => res.json())
    .then(data => dispatch({
      type: 'FETCH_RESULTS_ALBUMS',
      data
    }))
}