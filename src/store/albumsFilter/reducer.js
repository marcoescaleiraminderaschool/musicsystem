const initialState = { 
  text: '', 
  sortBy: '', 
  data: [] 
};

const albumsFilterReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'SET_FILTER_ALBUMS':
      return {
        ...state,
        text: action.text,
        sortBy: action.sortBy
      }
    case 'FETCH_RESULTS_ALBUMS':
      return {
        ...state,
        data: action.data
      }
    default:
      return state;
  }
};

export default albumsFilterReducer;