export const getTextFilter = (state) => state.albumsFilter.text;

export const getOrderCriteria = (state) => state.albumsFilter.sortBy;
 
export const getResults = (state) => state.albumsFilter.data;