import { API_URL, HEADERS } from "../../config";
import { getAllArtists } from "../artists/selectors";

export const loadArtists = () => (dispatch, getState) => {
  const isInCache = getAllArtists(getState()).length > 0;

  if (isInCache) {
    return;
  }

  fetch(`${API_URL}/artists?_sort=name&_order=asc`)
    .then(res => res.json())
    .then(artists => {
      dispatch({
        type: 'LOAD_ARTISTS',
        artists
      });
    });
};

// ADD_GENRE
export const addArtist = (name = '') => (dispatch) => {
  fetch(`${API_URL}/artists`, {
    headers: HEADERS,
    method: 'POST',
    body: JSON.stringify(name)
  })
    .then(res => res.json())
    .then(artist => {
      dispatch({
        type: 'ADD_ARTIST',
        artist
      });
    });
};

// REMOVE_GENRE
export const removeArtist = (id) => (dispatch) => {
  fetch(`${API_URL}/artists/${id}`, {
    method: 'DELETE'
  }).then(() => {
    dispatch({
      type: 'REMOVE_ARTIST',
      id
    });
  });
};

// EDIT_GENRE
export const editArtist = (id, updates) => (dispatch) => {
  fetch(`${API_URL}/artists/${id}`, {
    headers: HEADERS,
    method: 'PUT',
    body: JSON.stringify(updates)
  })
    .then(res => res.json())
    .then(updatedArtist => {
      dispatch({
        type: 'EDIT_ARTIST',
        updatedArtist
      })
    });
};