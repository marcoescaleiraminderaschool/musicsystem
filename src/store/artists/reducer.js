const artistsReducer = (state = [], action) => {
  switch (action.type) {
    case 'LOAD_ARTISTS':
      return action.artists;
    case 'ADD_ARTIST':
      return [
        ...state,
        action.artist
      ];
    case 'REMOVE_ARTIST':
      return state.filter(({ id }) => id !== action.id);
    case 'EDIT_ARTIST':
      return state.map((artist) => {
        if(artist.id === action.updatedArtist.id) {
          return {
            ...artist,
            ...action.updatedArtist
          };
        } else {
          return artist;
        }
      });
    default:
      return state;
  }
};

export default artistsReducer;