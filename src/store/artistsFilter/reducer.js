const initialState = { 
  text: '',
  data: []
};

const artistsFilterReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'SET_FILTER_ARTISTS':
      return {
        ...state,
        text: action.text
      }
    case 'FETCH_RESULTS_ARTISTS':
      return {
        ...state,
        data: action.data
      }
    default:
      return state;
  }
};

export default artistsFilterReducer;