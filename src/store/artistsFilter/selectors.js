export const getTextFilter = (state) => state.artistsFilter.text;
 
export const getResults = (state) => state.artistsFilter.data;