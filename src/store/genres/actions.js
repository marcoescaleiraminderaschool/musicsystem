import { API_URL, HEADERS } from "../../config";
import { getAllGenres } from "./selectors";

export const loadGenres = () => (dispatch, getState) => {
  const isInCache = getAllGenres(getState()).length > 0;

  if (isInCache) {
    return;
  }

  fetch(`${API_URL}/genres?_sort=name&_order=asc`)
    .then(res => res.json())
    .then(genres => {
      dispatch({
        type: 'LOAD_GENRES',
        genres
      });
    });
};

// ADD_GENRE
export const addGenre = (name = '') => (dispatch) => {
  fetch(`${API_URL}/genres`, {
    headers: HEADERS,
    method: 'POST',
    body: JSON.stringify(name)
  })
    .then(res => res.json())
    .then(genre => {
      dispatch({
        type: 'ADD_GENRE',
        genre
      });
    });
};

// REMOVE_GENRE
export const removeGenre = (id) => (dispatch) => {
  fetch(`${API_URL}/genres/${id}`, {
    method: 'DELETE'
  }).then(() => {
    dispatch({
      type: 'REMOVE_GENRE',
      id
    });
  });
};

// EDIT_GENRE
export const editGenre = (id, updates) => (dispatch) => {
  fetch(`${API_URL}/genres/${id}`, {
    headers: HEADERS,
    method: 'PUT',
    body: JSON.stringify(updates)
  })
    .then(res => res.json())
    .then(updatedGenre => {
      dispatch({
        type: 'EDIT_GENRE',
        updatedAlbum: updatedGenre
      })
    });
};