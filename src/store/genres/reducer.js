const genresReducer = (state = [], action) => {
  switch (action.type) {
    case 'LOAD_GENRES':
      return action.genres;
    case 'ADD_GENRE':
      return [
        ...state,
        action.genre
      ];
    case 'REMOVE_GENRE':
      return state.filter(({ id }) => id !== action.id);
    case 'EDIT_GENRE':
      return state.map((genre) => {
        if(genre.id === action.updatedAlbum.id) {
          return {
            ...genre,
            ...action.updatedAlbum
          };
        } else {
          return genre;
        }
      });
    default:
      return state;
  }
};

export default genresReducer;