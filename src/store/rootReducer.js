import { combineReducers } from "redux";

import tracksReducer from "./tracks";
import tracksFilterReducer from "./tracksFilter/reducer";

import artistsReducer from "./artists";
import artistsFilterReducer from "./artistsFilter/reducer";

import albumsReducer from "./albums";
import albumsFilterReducer from "./albumsFilter";

import genresReducer from "./genres";

export default combineReducers({
  tracks: tracksReducer,
  tracksFilter: tracksFilterReducer,
  artists: artistsReducer,
  artistsFilter: artistsFilterReducer,
  albums: albumsReducer,
  albumsFilter: albumsFilterReducer,
  genres: genresReducer
});