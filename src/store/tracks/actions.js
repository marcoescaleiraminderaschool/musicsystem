import { API_URL, HEADERS } from "../../config";
import { getAllTracks } from "./selectors";

export const loadTracks = () => (dispatch, getState) => {
  const isInCache = getAllTracks(getState()).length > 0;

  if (isInCache) {
    return;
  }

  fetch(`${API_URL}/tracks?_sort=name&_order=asc`)
    .then(res => res.json())
    .then(tracks => {
      dispatch({
        type: 'LOAD_TRACKS',
        tracks
      });
    });
};

// ADD_TRACK
export const addTrack = ({name = '', duration = 0, albumId, artistId = [], genreId} = {}) => (dispatch) => {
  fetch(`${API_URL}/tracks`, {
    headers: HEADERS,
    method: 'POST',
    body: JSON.stringify({
      name,
      duration,
      albumId,
      artistId,
      genreId
    })
  })
    .then(res => res.json())
    .then(track => {
      dispatch({
        type: 'ADD_TRACK',
        track
      });
    });
};

// REMOVE_TRACK
export const removeTrack = (id) => (dispatch) => {
  fetch(`${API_URL}/tracks/${id}`, {
    method: 'DELETE'
  }).then(() => {
    dispatch({
      type: 'REMOVE_TRACK',
      id
    });
  });
};

// EDIT_TRACK
export const editTrack = (id, updates) => (dispatch) => {
  fetch(`${API_URL}/tracks/${id}`, {
    headers: HEADERS,
    method: 'PUT',
    body: JSON.stringify(updates)
  })
    .then(res => res.json())
    .then(updatedTrack => {
      dispatch({
        type: 'EDIT_TRACK',
        updatedTrack
      })
    });
};