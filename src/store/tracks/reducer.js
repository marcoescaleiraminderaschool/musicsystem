const tracksReducer = (state = [], action) => {
  switch (action.type) {
    case 'LOAD_TRACKS': 
      return action.tracks;
    case 'ADD_TRACK':
      return [
        ...state,
        action.track
      ];
    case 'REMOVE_TRACK':
      return state.filter(({ id }) => id !== action.id);
    case 'EDIT_TRACK':
      return state.map((track) => {
        if(track.id === action.updatedTrack.id) {
          return {
            ...track,
            ...action.updatedTrack
          };
        } else {
          return track;
        }
      });
    default:
      return state;
  }
};

export default tracksReducer;