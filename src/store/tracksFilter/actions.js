import { API_URL } from '../../config';

export const setFilter = (text) => (dispatch) => {
  dispatch({
    type: 'SET_FILTER_TRACKS',
    text
  });

  dispatch(fetchResults(text));
};

export const fetchResults = (text) => (dispatch) => {
  fetch(`${API_URL}/tracks?q=${text}&_order=asc`)
    .then(res => res.json())
    .then(data => dispatch({
      type: 'FETCH_RESULTS_TRACKS',
      data
    }))
}