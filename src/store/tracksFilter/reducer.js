const initialState = { 
  text: '',
  data: []
};

const tracksFilterReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'SET_FILTER_TRACKS':
      return {
        ...state,
        text: action.text
      }
    case 'FETCH_RESULTS_TRACKS':
      return {
        ...state,
        data: action.data
      }
    default:
      return state;
  }
};

export default tracksFilterReducer;