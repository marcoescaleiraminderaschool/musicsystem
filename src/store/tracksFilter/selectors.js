export const getTextFilter = (state) => state.tracksFilter.text;
 
export const getResults = (state) => state.tracksFilter.data;